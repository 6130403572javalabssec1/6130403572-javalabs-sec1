/**
* This Java program is SortNumbers that accept five decimals, 
* read and store those numbers in a numberArr, and then sort these numbers.
* Check if the number of arguments is five and show sort numbers.
* If not the output of the program is in the format
* "Usage: SortNumbers <number1> <number2> <number3> <number4> <number5>"
*
* Author: Parida Seehamart
* ID: 613040357-2
* Sec: 1
* Date: January 27, 2019
*
**/

package seehamart.parida.lab2;
		
import java.util.Arrays;

public class SortNumbers {
   public static void main(String[] args) {
	  if (args.length != 5) {
			  System.err.println("Usage: SortNumbers <number1> <number2> <number3> <number4> <number5>");
		
		} else {
			  
			double number1  = Double.parseDouble(args[0]);
			double number2  = Double.parseDouble(args[1]);
			double number3  = Double.parseDouble(args[2]);
			double number4  = Double.parseDouble(args[3]);
			double number5  = Double.parseDouble(args[4]);
			  
			// initializing unsorted double array
		    double numberArr[] = {number1, number2, number3, number4, number5};
			  
		    // sorting array
		    Arrays.sort(numberArr);
		    // let us print all the elements available in list
		    for (double number : numberArr) {
		       System.out.print(number + " ");
      
		      }
		}
   }
}

		
		
		
		
		
		
		
		

			
		
