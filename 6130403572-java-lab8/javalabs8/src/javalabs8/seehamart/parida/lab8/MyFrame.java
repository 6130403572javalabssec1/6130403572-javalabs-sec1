package javalabs8.seehamart.parida.lab8;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;


public class MyFrame extends JFrame{


	public MyFrame(String string) {
		// TODO Auto-generated constructor stub
		super(string);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
				}
		});
	}

	public static void createAndShowGUI() {
		// create GUI
		MyFrame msw = new MyFrame("My Frame");
		msw.addComponens();
		msw.setFrameFeatures();
	}

	
	protected void addComponens() {
		// TODO Auto-generated method stub
		add(new MyCanvas());
	}
	
	protected void setFrameFeatures() {
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

	
	
	
	
}
