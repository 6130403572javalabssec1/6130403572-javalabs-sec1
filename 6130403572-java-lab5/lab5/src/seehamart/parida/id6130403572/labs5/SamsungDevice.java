package seehamart.parida.id6130403572.labs5;

public class SamsungDevice extends MobileDevices {

	private String modelName;
	private static String os;
	private int price;
	private int weight;
	private double version;
	

	public double getVersion() {
		return version;
	}


	public void setVersion(double version) {
		this.version = version;
	}


	public SamsungDevice(String modelName, int price, int weight, double version) {
		super(modelName, os, price, weight);
		// TODO Auto-generated constructor stub
		this.modelName = modelName;
		this.os = "Android";
		this.price = price;
		this.weight = weight;
		this.version = version;
		
	}
	
	public SamsungDevice(String modelName, int price, double version) {
		super(modelName, os, price);
		// TODO Auto-generated constructor stub
		this.modelName = modelName;
		this.os = "Android";
		this.price = price;
		this.version = version;
		
	}
	@Override
	public String toString() {
		return "SamsungDevice [modelName = " + modelName + ", OS = " + os + ", Price = " + price + " Baht, Weight = " + weight
				+ " g, Android version = " + version + " ]";
	}

	public static String getBrand() {
		// TODO Auto-generated method stub
		String brand = "Samsung";
		return brand;
	}
	

}
