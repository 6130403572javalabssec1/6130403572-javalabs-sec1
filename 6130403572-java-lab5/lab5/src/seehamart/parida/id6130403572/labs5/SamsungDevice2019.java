package seehamart.parida.id6130403572.labs5;

public class SamsungDevice2019 extends SamsungDevice {


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MobileDevices s9 = new SamsungDevice("Galaxy S9", 23900, 163, 8.0);
		SamsungDevice note9 = new SamsungDevice("Galaxy Note 9", 25500, 201, 8.1);
		System.out.println("I would like to have ");
		System.out.println(note9);
		System.out.println("But to save money, I should buy ");
		System.out.println(s9);
		double diff = note9.getPrice()- s9.getPrice();
		System.out.println("Samsung Galaxy Note 9 is more expensive than Samsung Galaxy S9 by " + diff + " Baht");
		System.out.println("Both these devices have the same brand which is " + SamsungDevice.getBrand());
	}

	
}
