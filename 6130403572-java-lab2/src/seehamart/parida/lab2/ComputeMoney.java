/**
* This Java program that accepts 4 arguments:the number of notes of 1,000 Baht, 500 Baht, 100 Baht and 20 Baht.
* total is the sum of the number of notes multiplied by each value of that note. 
* The output of the program is in the format
* "Total money is " + total
*
* Author: Parida Seehamart
* ID: 613040357-2
* Sec: 1
* Date: January 27, 2019
*
**/

package seehamart.parida.lab2;

public class ComputeMoney {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		if (args.length != 4) {
			System.err.println("Usage: ComputeMoney <1,000 Baht> <500 Baht> <100 Baht> <20 Baht>");
		
		} else {
			double thousand  = Double.parseDouble(args[0]);
			double fiveHundred  = Double.parseDouble(args[1]);
			double oneHundred = Double.parseDouble(args[2]);
			double twenty  = Double.parseDouble(args[3]);
			double total = (thousand*1000) + (fiveHundred*500) + (oneHundred*100) + (twenty*20);
			System.out.println("Total money is  " + total);
		}
		

	}

}
