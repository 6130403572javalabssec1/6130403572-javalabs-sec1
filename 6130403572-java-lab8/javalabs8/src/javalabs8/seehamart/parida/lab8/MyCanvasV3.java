package javalabs8.seehamart.parida.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class MyCanvasV3 extends MyCanvasV2 {
	
	protected MyCanvasV3() {
		// TODO Auto-generated constructor stub
		super();}

	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.BLACK);
	    Graphics2D g2d = (Graphics2D)g;
	    
	    int width = MyBrick.brickWidth;
	    for (int i=0; i<12; i++) {
		    	Rectangle2D.Double brick = new  Rectangle2D.Double(i, 290, MyBrick.brickWidth, MyBrick.brickHeight);
	    		brick.x = 0 + i*width;
			    g2d.setColor(Color.WHITE);
	    		g2d.fill(brick);
			    g2d.setColor(Color.BLACK); 
			    g2d.setStroke(new BasicStroke (5));
		    	g2d.draw(brick);
		
	    }
	    g2d.setColor(Color.WHITE);
	    g2d.fillOval(0, 570, MyBall.diameter, MyBall.diameter);
	    g2d.fillOval(770, 570, MyBall.diameter, MyBall.diameter); 
	    g2d.fillOval(0, 0, MyBall.diameter, MyBall.diameter); 
	    g2d.fillOval(770, 0, MyBall.diameter, MyBall.diameter); 
	    
	}
		
	
}
