package javalabs8.seehamart.parida.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV2 extends MyFrame{

	public MyFrameV2(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
				}
		});
	}

	public static void createAndShowGUI() {
		// create GUI
		MyFrameV2 msw = new MyFrameV2("My Frame V2");
		msw.addComponens();
		msw.setFrameFeatures();
	}

	
	protected void addComponens() {
		// TODO Auto-generated method stub
		add(new MyCanvasV2());
	}
}
