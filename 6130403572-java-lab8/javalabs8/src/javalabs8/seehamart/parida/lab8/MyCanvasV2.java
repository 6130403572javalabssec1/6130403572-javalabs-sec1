package javalabs8.seehamart.parida.lab8;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class MyCanvasV2 extends MyCanvas {		
	
	protected MyCanvasV2() {
		// TODO Auto-generated constructor stub
		super();
		
}
	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.BLACK);
	    Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.WHITE);        
	    Rectangle2D.Double brick = new  Rectangle2D.Double(360, 0, MyBrick.brickWidth, MyBrick.brickHeight);
	    g2d.fill(brick);
	    g2d.fillOval(385, 285, MyBall.diameter, MyBall.diameter); 
	    Rectangle2D.Double pedal = new  Rectangle2D.Double(350, 590, MyPedal.pedalWidth, MyPedal.pedalHeight);
	    g2d.fill(pedal);
	    g2d.drawLine(400, 0, 400, 600);
	    g2d.drawLine(0, 300, 800, 300);
	   
	    
	    
	}
	
}
