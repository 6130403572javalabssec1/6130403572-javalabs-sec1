package javalab6.seehamart.parida.lab6;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV4 extends MobileDeviceFormV3{
	protected JMenu itemMenuTwo1AddSub;
	protected JMenu itemMenuTwo2AddSub;
	
	public MobileDeviceFormV4(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	protected void addMenus() {
		super.addMenus();
		ubdateMenuIcon();
		addSubMenus();
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	public static void createAndShowGUI() {
		MobileDeviceFormV4 mobileDeviceFormV4 = new MobileDeviceFormV4("Mobile Device Form V4");
		mobileDeviceFormV4.addComponents();
		mobileDeviceFormV4.addMenus();
		mobileDeviceFormV4.setFrameFeatures();
	}
	private void addSubMenus() {
		// TODO Auto-generated method stub
		menu2.remove(itemMenuTwo1);
		menu2.remove(itemMenuTwo2);
		itemMenuTwo1AddSub = new JMenu("Color");
		itemMenuTwo1AddSub.add(new JMenuItem("Red"));
		itemMenuTwo1AddSub.add(new JMenuItem("Green"));
		itemMenuTwo1AddSub.add(new JMenuItem("Blue"));
		menu2.add(itemMenuTwo1AddSub);
		
		itemMenuTwo2AddSub = new JMenu("Size"); 
		itemMenuTwo2AddSub.add(new JMenuItem("16"));
		itemMenuTwo2AddSub.add(new JMenuItem("20"));
		itemMenuTwo2AddSub.add(new JMenuItem("24"));
		menu2.add(itemMenuTwo2AddSub);
		
	}

	private void ubdateMenuIcon() {
		// TODO Auto-generated method stub
		itemMenuOne1.setIcon ( new ImageIcon ("icon/new2.png") );

		
	}
	
	
	
}
