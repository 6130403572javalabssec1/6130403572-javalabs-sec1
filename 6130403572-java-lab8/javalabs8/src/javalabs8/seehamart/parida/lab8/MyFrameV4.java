package javalabs8.seehamart.parida.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV4 extends MyFrameV3{

	public MyFrameV4(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
				}
		});
	}

	public static void createAndShowGUI() {
		// create GUI
		MyFrameV4 msw = new MyFrameV4("My Frame V4");
		msw.addComponens();
		msw.setFrameFeatures();
	}

	
	protected void addComponens() {
		// TODO Auto-generated method stub
			add(new MyCanvasV4());
		}

	
}
