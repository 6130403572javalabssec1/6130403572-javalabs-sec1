package seehamart.parida.lab4;

public interface Movable {
	 void accelerate();
	 void brake();
	 void setSpeed();

}
