package seehamart.parida.lab4;

class ToyotaAuto extends Automobile implements Movable, Refuelable {

	public ToyotaAuto(int maximumSpeed, int acceleration, String model) {
		// TODO Auto-generated constructor stub
		this.gasoline = 100;
		this.maxSpeed = maximumSpeed;
		this.model = model;
		this.acceleration = acceleration;
	}


	@Override
	public void accelerate() {
		// TODO Auto-generated method stub
		this.speed += getAcceleration();
		this.gasoline -= 15;
		System.out.println(model + " accelerates");
		
	}

	@Override
	public void brake() {
		// TODO Auto-generated method stub
		if (speed != 0) {
			this.speed -= getAcceleration();
		} else {
			speed = 0;
		}
		this.gasoline -= 15;
		System.out.println(model +" brakes");
	}

	@Override
	public void setSpeed() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void refuel() {
		// TODO Auto-generated method stub
		this.gasoline = 100;
		System.out.println(model + " refuels");
	}

	public void refuel(int gasoline) {
		// TODO Auto-generated method stub
		this.gasoline = 100;
	}

	


	
}