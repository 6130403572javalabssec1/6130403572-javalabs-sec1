package javalab6.seehamart.parida.lab6;


import java.awt.*;
import javax.swing.*;

public class  MySimpleWindow extends JFrame {
	protected JPanel bottonPanel;
	protected JButton cancel;
	protected JButton ok;
	protected JFrame window;
	protected JPanel mainPanel;
	

	public MySimpleWindow(String title){
		super(title);
		// TODO Auto-generated constructor stub
	}

	public MySimpleWindow() {
		// TODO Auto-generated constructor stub
	}


	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("MSW");
		msw.addComponents();
		msw.setFrameFeatures();
		
		}
		

	protected void setFrameFeatures() {
		// TODO Auto-generated method stub
		window = new JFrame("My Simple Window");
	    window.setLocationRelativeTo(null);
	    window.setVisible(true);
	    window.add(mainPanel);
	    window.pack();
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	}


	protected void addComponents() {
		// TODO Auto-generated method stub
		mainPanel = new JPanel();
		bottonPanel = new JPanel(new FlowLayout());
		cancel = new JButton("cancel");
		ok = new JButton("ok");
		
		bottonPanel.add(cancel);
		bottonPanel.add(ok);
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(bottonPanel);
	}


	public static void main(String[] args) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
					}
				});
		}
	
	
}
