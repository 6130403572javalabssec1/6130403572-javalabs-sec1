package seehamart.parida.id6130403572.labs5;

public class AndroidSmartWatch extends AndroidDevice {

	
	private String name;
	private String model;
	private int price;
	
	public AndroidSmartWatch(String name, String model, int price) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.model = model;
		this.price = price;
		
	}
	@Override
	public String toString() {
		return "AndroidSmartWatch [name = " + name + ", model name = " + model + ", price = " + price + " Baht]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	@Override
	public void usage() {
		// TODO Auto-generated method stub
		
	}


}
