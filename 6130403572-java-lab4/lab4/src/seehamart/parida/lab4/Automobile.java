package seehamart.parida.lab4;


public class Automobile {
    protected int gasoline;    
    protected int speed;  
    protected int maxSpeed;
    protected int acceleration;
    protected String model;
    private Color color;
    static int numberOfAutomobile = 0;
    
    public int getGasoline() {
		return gasoline;
	}
	public void setGasoline(int gasoline) {
		this.gasoline = gasoline;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public int getAcceleration() {
		return acceleration;
	}
	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public static int getNumberOfAutomobile() {
		return numberOfAutomobile;
	}
	public void setNumberOfAutomobile(int numberOfAutomobile) {
		Automobile.numberOfAutomobile = numberOfAutomobile;
	}  
		
	
	public Automobile(int gasoline, int speed, int maxSpeed, int acceleration, String model, Color color) {
    	this.gasoline = gasoline;
    	this.speed = speed;
    	this.acceleration = acceleration;
    	this.maxSpeed = maxSpeed;
    	this.model = model;
    	this.color = color;
    	this.setNumberOfAutomobile(numberOfAutomobile+1);
	}
	public Automobile() {
    	gasoline = 0;
    	speed = 0;
    	acceleration = 0;
    	maxSpeed = 160;
    	model = "Automobile";
    	color = Color.WHITE;
    	this.setNumberOfAutomobile(numberOfAutomobile+1);	
    }
	@Override
	public String toString() {
		return "Automobile [gasoline=" + gasoline + ", speed=" + speed + ", maxSpeed=" + maxSpeed + ", acceleration="
				+ acceleration + ", model=" + model + ", color=" + color + "]";
	}
    
}    
    
    
    
    
    
    