package javalab6.seehamart.parida.lab6;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.MenuItem;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV3 extends MobileDeviceFormV2 {

	protected JMenuBar menuBar;
	protected JMenu menu1;
	protected JMenu menu2;
	protected JMenuItem itemMenuOne1;
	protected JMenuItem itemMenuOne2;
	protected JMenuItem itemMenuOne3;
	protected JMenuItem itemMenuOne4;
	protected JMenuItem itemMenuTwo1;
	protected JMenuItem itemMenuTwo2;
	protected JList featuresList;
	protected JPanel southPanel;
	protected JPanel FPanel;
	protected JPanel reviewPanel;
	protected JLabel features;
	
	public MobileDeviceFormV3(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	public static void createAndShowGUI() {
		MobileDeviceFormV3 mobileDeviceFormV3 = new MobileDeviceFormV3("Mobile Device Form V3");
		mobileDeviceFormV3.addComponents();
		mobileDeviceFormV3.addMenus();
		mobileDeviceFormV3.setFrameFeatures();
	}
	protected void addMenus() {
		// TODO Auto-generated method stub
		menuBar = new JMenuBar();
		menu1 = new JMenu("File");
		menuBar.add(menu1);
		
		
		itemMenuOne1 = new JMenuItem("New"); 
		itemMenuOne2 = new JMenuItem("Open");  
		itemMenuOne3 = new JMenuItem("Save");  
		itemMenuOne4 = new JMenuItem("Exit");  
	
        menu1.add(itemMenuOne1); 
        menu1.add(itemMenuOne2); 
        menu1.add(itemMenuOne3);  
        menu1.add(itemMenuOne4); 
        
        menu2 = new JMenu("Config");
		menuBar.add(menu2);
		
        itemMenuTwo1 = new JMenuItem("Color"); 
		itemMenuTwo2 = new JMenuItem("Size");  
		
		menu2.add(itemMenuTwo1); 
	    menu2.add(itemMenuTwo2); 
		
	}
	protected void setFrameFeatures() {
		// TODO Auto-generated method stub
		MobileDeviceFormV3 window = new MobileDeviceFormV3("Mobile Device Form V3");
		this.setLocationRelativeTo(null);
		this.add(mainPanel);
		this.setVisible(true);
		this.setJMenuBar(menuBar);
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	}

	protected void addComponents() {
		// TODO Auto-generated method stub
		super.addComponents();
		/**
		 * Add the list with the label �Features:�
		 * the choices as 
		 * �Design and build quality�, 
		 * �Great Camera�, 
		 * �Screen�, 
		 * �Battery Life�
		 */
		
		FPanel = new JPanel(new BorderLayout());
		features = new JLabel("Features:");
		DefaultListModel showFeatures = new DefaultListModel();
		showFeatures.addElement("Design and build quality");
		showFeatures.addElement("Great Camera");
		showFeatures.addElement("Screen");
		showFeatures.addElement("Battery Life");
		featuresList = new JList(showFeatures);
		FPanel.add(features, BorderLayout.WEST);
		FPanel.add(featuresList, BorderLayout.EAST);	
		
		reviewPanel = new JPanel(new FlowLayout());
		reviewPanel.add(review);
		
		V1Panel.remove(review);
		midPanel.remove(scroll);
		midPanel.add(FPanel, BorderLayout.NORTH);
		midPanel.add(review, BorderLayout.CENTER);
		midPanel.add(scroll, BorderLayout.SOUTH);
		
        
        southPanel = new JPanel(new BorderLayout());
        southPanel.add(bottonPanel);
        
        mainPanel.removeAll();	
        mainPanel.add(northPanel, BorderLayout.NORTH);
		mainPanel.add(midPanel, BorderLayout.CENTER);
        mainPanel.add(southPanel, BorderLayout.SOUTH);
	}
	
	
	
}
