package javalabs8.seehamart.parida.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class MyCanvasV4 extends MyCanvasV3{
	protected MyCanvasV4() {
		// TODO Auto-generated constructor stub
		super();}
	
	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
	    Graphics2D g2d = (Graphics2D)g;
	    setBackground(Color.BLACK);
	    g2d.setStroke(new BasicStroke (5));
	    Color[] color = {Color.RED, Color.YELLOW, Color.BLUE, Color.MAGENTA,Color.GREEN, Color.CYAN,Color.WHITE };
		
	    int width = MyBrick.brickWidth;
		int height = MyBrick.brickHeight;
		
		Rectangle2D.Double brick = new  Rectangle2D.Double(0,0, MyBrick.brickWidth,MyBrick.brickHeight);
	    for (int i=0; i<12; i++) {
	    	for (int j=0; j<7; j++) {	    	
		    	brick.x = 0 + i*width;
		    	brick.y = 50 + j*height;
		    	g2d.setColor(color[j]);
		    	g2d.fill(brick);
		    	g2d.setColor(Color.BLACK); 
		    	g2d.draw(brick);
		    	
	    	}
	    }
	    
	    Rectangle2D.Double pedal = new  Rectangle2D.Double(350, 590, MyPedal.pedalWidth, MyPedal.pedalHeight);
	    g2d.setColor(Color.GRAY);
	    g2d.fill(pedal);
	    g2d.setColor(Color.WHITE);
	    g2d.fillOval(385, 560, MyBall.diameter, MyBall.diameter); 
	}
}
