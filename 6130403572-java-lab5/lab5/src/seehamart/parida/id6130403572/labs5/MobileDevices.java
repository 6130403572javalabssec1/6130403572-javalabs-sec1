/**
 * MobileDevice is to modela mobile device object with
 * attributes modelName, os, price, and weight
 * 
 * @author Parida Seehamart
 * @version 1.0
 * @since 2019-02-18
 * 
 */

package seehamart.parida.id6130403572.labs5;

public class MobileDevices {
	private String modelName;
	private String os;
	private int price;
	private int weight;
	@Override
	public String toString() {
		return "MobileDevices [modelName=" + modelName + ", os=" + os + ", price=" + price + ", weight=" + weight + " g]";
	}

	public MobileDevices(String modelName, String os, int price, int weight) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = weight;
	}
	
	public MobileDevices(String modelName, String os, int price) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = 0;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}


	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
	


}
