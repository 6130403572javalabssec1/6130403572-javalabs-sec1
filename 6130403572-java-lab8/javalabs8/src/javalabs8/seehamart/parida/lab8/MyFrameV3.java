package javalabs8.seehamart.parida.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV3 extends MyFrameV2{

	public MyFrameV3(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
				}
		});
	}

	public static void createAndShowGUI() {
		// create GUI
		MyFrameV3 msw = new MyFrameV3("My Frame V3");
		msw.addComponens();
		msw.setFrameFeatures();
	}

	
	protected void addComponens() {
		// TODO Auto-generated method stub
		add(new MyCanvasV3());
	}

}
