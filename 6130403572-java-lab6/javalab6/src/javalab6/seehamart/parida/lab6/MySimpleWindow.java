package javalab6.seehamart.parida.lab6;


import java.awt.*;
import javax.swing.*;

public class  MySimpleWindow extends JFrame {
	protected JPanel panel;
	protected JButton cancle;
	protected JButton ok;
	protected JFrame window;

	public MySimpleWindow(String title){
		super(title);
		// TODO Auto-generated constructor stub
	}


	public MySimpleWindow() {
		// TODO Auto-generated constructor stub
	}


	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("MSW");
		msw.addComponents();
		msw.setFrameFeatures();
		msw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		}
		

	protected void setFrameFeatures() {
		// TODO Auto-generated method stub
		window = new JFrame("My Simple Window");
	    window.setLocationRelativeTo(null);
	    window.setVisible(true);
	    window.add(panel);
	    window.pack();
	}


	protected void addComponents() {
		// TODO Auto-generated method stub
		panel = new JPanel(new FlowLayout());
		cancle = new JButton("cancle");
		ok = new JButton("ok");
		
		setLayout(new BorderLayout());
		add(panel, BorderLayout.EAST);
		add(panel, BorderLayout.WEST);
		
		panel.add(cancle);
		panel.add(ok);
	}


	public static void main(String[] args) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					createAndShowGUI();
					}
				});
		}
	
	
}
