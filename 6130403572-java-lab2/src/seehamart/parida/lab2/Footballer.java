/**
* This Java program that accepts three arguments: your favorite football player, 
* the football club that the player plays for, and the nationality of that player. 
* The output of the program is in the format
* "My favorite football player is " + <footballer name>
* "His nationality is " + <nationality>
* "He plays for " + <football club>
*
* Author: Parida Seehamart
* ID: 613040357-2
* Sec: 1
* Date: January 27, 2019
*
**/

package seehamart.parida.lab2;

public class Footballer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if (args.length != 3) {
			System.err.println("Usage: Footballer <footballer name> <nationality> <football club>");
		
		} else {
			System.out.println("My favorite football player is " + args[0]);
			System.out.println("His nationality is " + args[1]);
			System.out.println("He plays for " + args[2]);
		}

	
	}

}
