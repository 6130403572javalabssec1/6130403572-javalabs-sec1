/**
* This Java program is DataTypes that declare the following variable in the given types and conditions:
*1. A String variable with value = myName
*2. A String variable with value = myID
*3. A char variable with value = myFirstLetter
*4. A boolean variable with value = myBool
*5. An int variable with value = theLastTwoDigitsOfYourlIDNumberOct
*6. An int variable with value = theLastTwoDigitsOfYourlIDNumberHex
*7. A long variable with value = theLastTwoDigitsOfYourIDNumberLong 
*8. A float variable with value = theLastTwoDigitsOfYourIDNumberFloat 
*9. A double variable with value = theLastTwoDigitsOfYourIDNumberDouble 
*  The output of the program is in the format
*1 "My name is " + myName
*2 "My student ID is " + myID
*3 myFirstLetter + " "
*4 myBool + " "
*5 theLastTwoDigitsOfYourlIDNumberOct + " "
*6 theLastTwoDigitsOfYourlIDNumberHex
*7 theLastTwoDigitsOfYourlIDNumberLong + " "
*8 theLastTwoDigitsOfYourlIDNumberFloat + " "
*9 theLastTwoDigitsOfYourlIDNumberDouble + " "
*
* Author: Parida Seehamart
* ID: 613040357-2
* Sec: 1
* Date: January 27, 2019
*
**/

package seehamart.parida.lab2;

public class DataTypes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String myName = "Parida Seehamart";
		System.out.println("My name is " + myName);
		String myID = "6130403572";
		System.out.println("My student ID is " + myID);
		char myFirstLetter = 'P';  
		System.out.print(myFirstLetter + " ");
		boolean myBool = true;
		System.out.print(myBool + " ");
	
		//int theLastTwoDigitsOfYourlIDNumberOct = 110; 
		String octal = "110";
	    Integer theLastTwoDigitsOfYourlIDNumberOct = Integer.parseInt(octal,8);
	    System.out.print(+theLastTwoDigitsOfYourlIDNumberOct + " ");
		//int theLastTwoDigitsOfYourlIDNumberHex = 48; 
		String hex = "48";
        Integer theLastTwoDigitsOfYourlIDNumberHex = Integer.parseInt(hex,16);
        System.out.println(+theLastTwoDigitsOfYourlIDNumberHex);
		long theLastTwoDigitsOfYourlIDNumberLong = 72; 
		System.out.print(theLastTwoDigitsOfYourlIDNumberLong + " ");
		float theLastTwoDigitsOfYourlIDNumberFloat = 72.61f; 
		System.out.print(theLastTwoDigitsOfYourlIDNumberFloat + " ");
		double theFirstTwoDigitsOfYourIDNumberDouble = 72.61; 
		System.out.print(theFirstTwoDigitsOfYourIDNumberDouble + " ");
		
	}

}
