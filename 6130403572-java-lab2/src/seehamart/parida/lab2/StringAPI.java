/**
* This Java program is StringAPI that accepts one input argument <your school name>, 
* Save that argument in the variable called schoolName
* SchoolName has the substring “college” (ignore cases) and “university” (ignore cases).
* The display output as "
*
*
* Author: Parida Seehamart
* ID: 613040357-2
* Sec: 1
* Date: January 27, 2019
*
**/

package seehamart.parida.lab2;

public class StringAPI {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String schoolName = args[0];
		if (schoolName.contains("university")) {
	        System.out.println(schoolName + " is a university");
		} else if (schoolName.contains("college")){ 
			System.out.println(schoolName + " is a college");
		} else {
	        System.out.println(schoolName + " is neither a university nor a college");
	    }
			
			
	}

}
