package javalab6.seehamart.parida.lab6;

import javax.swing.*;
import java.awt.*;

public class MobileDeviceFormV1 extends MySimpleWindow {

	protected JPanel V1Panel;
	protected JPanel northPanel;
	protected JLabel  brandN;
	protected JLabel  modelN;
	protected JLabel  wKg;
	protected JLabel  price;
	protected JLabel  os;
	protected JTextField inputBN;
	protected JTextField inputMN;
	protected JTextField inputwKg;
	protected JTextField inputP;
	protected JRadioButton inputA;
	protected JRadioButton inputI;

	public MobileDeviceFormV1(String title){
		super(title);
		// TODO Auto-generated constructor stub
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV1 mobileDeviceFormV1 = new MobileDeviceFormV1("Mobile Device Form V1");
		mobileDeviceFormV1.addComponents();
		mobileDeviceFormV1.setFrameFeatures();
		
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void setFrameFeatures() {
		// TODO Auto-generated method stub
		MobileDeviceFormV1 window = new MobileDeviceFormV1("Mobile Device Form V1");
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.add(mainPanel);
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	}



	protected void addComponents() {
		// TODO Auto-generated method stub
		super.addComponents();
		
		V1Panel = new JPanel();
		brandN = new JLabel("Brand Name: ");
		inputBN = new JTextField(15);
		V1Panel.add(brandN);
		V1Panel.add(inputBN);
		
		modelN = new JLabel("Model Name: ");
		inputMN = new JTextField(15);
		V1Panel.add(modelN);
		V1Panel.add(inputMN);
		
		wKg = new JLabel("Weight (kg.): ");
		inputwKg = new JTextField(15);
		V1Panel.add(wKg);
		V1Panel.add(inputwKg);
		
		price = new JLabel("Price (Baht): ");
		inputP = new JTextField(15);
		V1Panel.add(price);
		V1Panel.add(inputP);
		
		JPanel allOS = new JPanel();
		os = new JLabel("Mobile OS: ");
		V1Panel.add(os);
		ButtonGroup group = new ButtonGroup();
		inputA = new JRadioButton("Android");
		inputI = new JRadioButton("IOS");
		group.add(inputA);
		group.add(inputI);
		allOS.add(inputA);
		allOS.add(inputI);
		V1Panel.add(allOS);
		V1Panel.setLayout(new GridLayout(0,2));
		
		northPanel = new JPanel(new BorderLayout());
		northPanel.add(V1Panel, BorderLayout.NORTH);
		
		
		mainPanel.add(northPanel, BorderLayout.NORTH);
		mainPanel.add(bottonPanel, BorderLayout.SOUTH);
		
	}
	
}
