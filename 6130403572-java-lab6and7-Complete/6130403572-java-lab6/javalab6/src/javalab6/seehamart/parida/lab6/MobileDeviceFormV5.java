package javalab6.seehamart.parida.lab6;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV5 extends MobileDeviceFormV4{

	protected JLabel lable;
	
	
	public MobileDeviceFormV5(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV5 mobileDeviceFormV5 = new MobileDeviceFormV5("Mobile Device Form V5");
		mobileDeviceFormV5.addComponents();
		mobileDeviceFormV5.addMenus();
		mobileDeviceFormV5.setFrameFeatures();
	}
	
	protected void addComponents() {
		// TODO Auto-generated method stub
		super.addComponents();
		
		//create new Font
		Font fontPlain = new Font("Serif", Font.PLAIN,14);
		Font fontBold = new Font("Serif", Font.BOLD,14);
		
		//set font for JLabel
		brandN.setFont(fontBold);
		modelN.setFont(fontBold); 
		wKg.setFont(fontBold); 
		price.setFont(fontBold);
		os.setFont(fontBold);
		type.setFont(fontBold);
		features.setFont(fontBold);
		review.setFont(fontBold);
		
		//set font for JTextField and JTextArea 
		inputBN.setFont(fontPlain);
		inputMN.setFont(fontPlain);
		inputwKg.setFont(fontPlain);
		inputP.setFont(fontPlain);
		reviews.setFont(fontPlain);
		
		//add color to button
		cancel.setForeground(Color.RED);
		ok.setForeground(Color.BLUE);
		
		//add photo
		ImageIcon addImage = new ImageIcon("icon/galaxyNote9.jpg");
	    lable = new JLabel(addImage);
	   
	    southPanel.add(lable, BorderLayout.NORTH);
	    southPanel.add(bottonPanel, BorderLayout.SOUTH);	
	    mainPanel.remove(bottonPanel);
	    mainPanel.add(midPanel);
	    mainPanel.add(southPanel, BorderLayout.SOUTH);
	}
	
}
